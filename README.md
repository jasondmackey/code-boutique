# Python code boutique
Python code [examples](src/), [tips and tricks](tips_and_tricks/) and essential Python [web links](web_links/)

Feel free to send me a pull request if you'd like to add or amend your own useful examples. Any such work will be attributed to you! And if you find this useful please [buy me a coffe](https://www.buymeacoffee.com/andykmiles)  <img src="_images/bmac.jpeg" alt="" width="20" height="20"> to help me with my costs. Thank you!
